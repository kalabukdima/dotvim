"{{{ Options
" General behaviour
set nocompatible
set magic
set encoding=utf-8 fileencoding=utf-8 termencoding=utf-8
set hidden " Automatically hide changed buffers when switching

" Search
set incsearch hlsearch nowrapscan
set ignorecase smartcase

" Interface
set number relativenumber ruler showcmd noshowmode
set textwidth=80 colorcolumn=80,120 linebreak
set foldlevel=9 foldopen-=hor
set scrolloff=3

" Command line
set wildmenu wildmode=longest,list " Unix-like tab behaviour
set history=128

" History
set undolevels=1024
set undofile
set undodir=~/.vim/.undo
set backupdir=/tmp
set directory=~/.vim/.swap

" Whitespace handling
set list listchars=tab:▸\ ,eol:¬
set backspace=indent,eol,start
set tabstop=8 softtabstop=4 shiftwidth=4 expandtab
set cinoptions=l1,+1s,N-s,(s,W1s,m1

"}}}


"{{{ Autocommands
augroup vimrc_autocmds
  autocmd!

  autocmd VimEnter * nested
  \ redraw | echo '>^.^<'

  autocmd InsertEnter * set number norelativenumber
  autocmd InsertLeave * set relativenumber

  " Return to last cursor position
  autocmd BufReadPost *
  \ if line("'\"") > 1 && line("'\"") <= line("$") |
  \   execute "normal! g`\"" |
  \ endif

  autocmd BufNewFile,BufRead /usr/include/c++/* setf cpp

  autocmd FileType gitcommit setlocal colorcolumn=50

  autocmd FileType tsv setlocal noexpandtab

  autocmd FileType cmake
  \ set commentstring=#\ %s

  autocmd FileType cpp
  \ setlocal commentstring=//\ %s |
  \ set foldmethod=syntax

  autocmd FileType rust nnoremap <leader>b :!cargo clippy<cr>
  autocmd FileType rust nnoremap <leader>t :!cargo test<cr>
  autocmd FileType rust nnoremap <leader>e :!cargo run<cr>
augroup END
"}}}


"{{{ Plugins
execute pathogen#infect()
execute pathogen#helptags()
filetype plugin indent on

"{{{ Solarized
set t_Co=256
let g:solarized_termcolors=256
let g:solarized_visibility = "low"
syntax on
set background=dark
colorscheme solarized
highlight ColorColumn ctermbg=237
"}}}
"{{{ Airline
set laststatus=2
if !exists('g:airline_extensions')
  let g:airline_extensions = ['whitespace', 'tabline']
endif
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.crypt = '$'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.notexists = '∄'
let g:airline_symbols.whitespace = 'Ξ'
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.spell = 's'
"}}}
"{{{ YouCompleteMe
let g:ycm_global_ycm_extra_conf = '~/.vim/.ycm_extra_conf.py'
let g:ycm_error_symbol = '#'
let g:ycm_warning_symbol = 'w'
let g:ycm_autoclose_preview_window_after_completion = 0
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_confirm_extra_conf = 0

" Compatibility with UltiSnips
let g:ycm_use_ultisnips_completer = 1
let g:ycm_key_list_select_completion = ['<tab>']
let g:ycm_key_list_previous_completion = ['<s-tab>']

let g:ycm_rust_src_path = systemlist('rustc --print sysroot')[0] . '/lib/rustlib/src/rust/src'
highlight link YcmWarningSection Normal
"}}}
"{{{ h2cppx
let g:h2cppx_python_path = '/usr/bin/python2.7'
let g:h2cppx_template = '/home/dima/.vim/h2cppx_templates/my_template'
"}}}
"{{{ UltiSnips
let g:UltiSnipsExpandTrigger="<c-e>"
let g:UltiSnipsJumpForwardTrigger="<c-f>"
let g:UltiSnipsJumpBackwardTrigger="<c-b>"
"}}}
"}}}


"{{{ Mappings
" Cancelling stuff
noremap <up> <nop>
inoremap <up> <nop>
noremap <down> <nop>
inoremap <down> <nop>
noremap <left> <nop>
inoremap <left> <nop>
noremap <right> <nop>
inoremap <right> <nop>
nnoremap <del> <nop>
inoremap <del> <nop>
nnoremap <F1> <nop>
inoremap <F1> <nop>
inoremap <F2> <nop>
inoremap <F3> <nop>
inoremap <F4> <nop>
inoremap <F5> <nop>
inoremap <F6> <nop>
inoremap <F7> <nop>
inoremap <F8> <nop>
inoremap <F9> <nop>
inoremap <F10> <nop>
inoremap <F11> <nop>
inoremap <F12> <nop>

inoremap <c-h> <nop>
inoremap <c-t> <nop>

" Fixing default behaviour
nnoremap <C-e> 3<C-e>
nnoremap <C-n> 3<C-y>
vnoremap <C-e> 3<C-e>
vnoremap <C-n> 3<C-y>
nnoremap Y y$
nnoremap Q @q

" 'Leading' mappings
nnoremap <leader>n :set rnu!<cr>
nnoremap <leader>y ggVG"+y``
nnoremap <leader>r :source $MYVIMRC<cr>
nnoremap <leader>v :tabedit $MYVIMRC<cr>
nnoremap <silent> <leader>c :let @/="\%$\%^"<cr>
nnoremap <leader>] :YcmCompleter GoTo<cr>
nnoremap <leader>d :e %:h<cr>
nnoremap <leader>f :YcmForceCompileAndDiagnostics<cr>

" F-keys
nnoremap <F2> :update<cr>
nnoremap <F5> :make! run<cr>
nnoremap <F7> :cnext<cr>
nnoremap <S-F7> :cprev<cr>
nnoremap <F10> :make!<cr>

" Others
nnoremap <space> za
nnoremap <S-Tab> gT
" Creates checkpoint - <c-i> doesn't lead anywhere after that.
nnoremap <cr> mQ'Q
inoremap <c-o> <c-r><c-o>"
" Use <c-o> in visual block mode to move the cursor to the column where I
" entered visual block mode.
nnoremap <c-v> mv<c-v>
vnoremap <c-o> oO`vo
" Split contents of () by commas in multiple lines
nnoremap gS ci)"k:s/, */,\r/g<cr>=a):let @/=""<cr>
nnoremap gN v%J%lx

" Workman layout optimizations
noremap n j
noremap N J
noremap e k
noremap E K
noremap k n
noremap K N
noremap j e
noremap J E
noremap l t
noremap L T
noremap t l
noremap T L
nnoremap <C-w>t <C-w>l
nnoremap gn gj
nnoremap ge gk
nnoremap zn zj
nnoremap ze zk
"}}}

runtime vimrc.local
" vim:foldmethod=marker:tabstop=2:softtabstop=2:shiftwidth=2
